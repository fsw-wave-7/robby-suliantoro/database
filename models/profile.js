'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Profile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Profile.belongsTo(models.User, {
        as: "Profile",
        foreignKey: "user_id",
        onDelete: "CASCADE",
      });

    }
  };
  Profile.init({
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    sex: DataTypes.STRING,
    ttl: DataTypes.STRING,
    user_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Profile',
  });
  return Profile;
};