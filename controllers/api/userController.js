const { User } = require('../../models')
const { Profile } = require('../../models') 
const { Posting } = require('../../models')
const { join } = require('path')
const bcrypt = require("bcrypt")

class UserController {
	getUser = (req, res) => {
		res.send("get");
	};
	insertUser = (req, res) => {
		res.send("insert");
	};
	updateUser = (req, res) => {
		res.send("update");
	};
	deleteUser = (req, res) => {
		res.send("delete");
	};
}

module.exports = UserController;
